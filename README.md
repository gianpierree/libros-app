## Libros CRUD

La solución está desarrollado en Laravel 8 y Vue 2 con Vuetify.

- Una vez clonado el repositorio se debe configurar los accesos de la BD en el archivo .env.
- Antes de levantar el servidor de Laravel, tiene que generar una key "php artisan key:generate".
- Después levante el servidor de laravel "php artian serve".
- Y por último, levante Vue "npm run dev".